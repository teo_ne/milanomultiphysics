# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.template import loader

from django.http import HttpResponse
from .models import Data

def index(request):
    # read all the available datas in the DB
    data_list = Data.objects.all()

    # load the template
    template = loader.get_template('data_list.html')

    # create a dictionary that is passed to the template
    context = {
        'data_list': data_list,
    }

    return HttpResponse(template.render(context, request))