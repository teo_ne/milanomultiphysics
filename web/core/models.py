# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Data(models.Model):
    field_one = models.CharField(max_length=200)
    field_two = models.IntegerField()