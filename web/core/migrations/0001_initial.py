# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-08 16:47
from __future__ import unicode_literals

from django.db import migrations, models
import os

class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    def generate_superuser(apps, schema_editor):
        from django.contrib.auth.models import User

        DJANGO_SU_NAME = os.environ.get('DJANGO_SU_NAME')
        DJANGO_SU_EMAIL = os.environ.get('DJANGO_SU_EMAIL')
        DJANGO_SU_PASSWORD = os.environ.get('DJANGO_SU_PASSWORD')

        print("Creating admin:\n")
        print("USERNAME: %s", DJANGO_SU_NAME)
        print("EMAIL: %s", DJANGO_SU_EMAIL)
        print("PASSWORD: %s", DJANGO_SU_PASSWORD)

        superuser = User.objects.create_superuser(
            username=DJANGO_SU_NAME,
            email=DJANGO_SU_EMAIL,
            password=DJANGO_SU_PASSWORD)

        superuser.save()

    operations = [
        migrations.RunPython(generate_superuser),
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('field_one', models.CharField(max_length=200)),
                ('field_two', models.IntegerField()),
            ],
        ),
    ]
