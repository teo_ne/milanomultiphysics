#!/usr/bin/env bash

sleep 1 # wait for db to start
python manage.py migrate
python /code/manage.py runserver 0.0.0.0:8000