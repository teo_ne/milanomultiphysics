# Django Setup

Basic project based on: https://docs.djangoproject.com/en/1.11/intro/

## Run it

You need to have `Docker` ([windows](https://docs.docker.com/docker-for-windows/install/), [mac](https://docs.docker.com/docker-for-mac/install/)) and [docker-compose](https://docs.docker.com/compose/install/) installed.

Once you have those, just run `make start` and visit:

- `localhost:8080/core` for the main page
- `localhost:8080/admin` for the admin panel (username: `admin`, password: `admin`)

## What it does

This examples create a setup with 3 containers (you can see them with `docker ps -a`):

- `db`: a PostgreSQL database
- `web`: a Django based application to expose some data (no authentication implemented yet)
- `worker`: a sample application that stores data in `db` so that they can be consumed by `web`

What happens is that:

- `db` comes online and stays there, ready for connections
- `web` comes online and start a webserver on port `8000`
- `worker` comes online, execute a script to generate sample data and then exits