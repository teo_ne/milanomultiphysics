import os

# tell Django wich settings module to use
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "milanomultiphysics.settings")

# import and setup Django
import django
django.setup()

# import the models
from core.models import Data

# create 10 random entries
for i in range(10):
    d = Data(field_one="foo-%s" % i, field_two=i)
    d.save()