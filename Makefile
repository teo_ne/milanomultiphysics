# start a development environment
start:
	docker-compose up --build

# deploy and stop
run:
	docker-compose up -d --build

stop:
	docker-compose down

# build the container (optional)
build-web:
	docker build -t mm/web:latest ./web

build-worker:
	docker build -t mm/worker:latest -f worker/Dockerfile .

# virtual env
venv:
	virtualenv django-venv
	@echo 'Activate the enviroment with "source django-venv/bin/activate" and then run "make dep" to install the required packages'

dep:
	pip install -r web/requirements.txt